/*
* Just an example for testing/using the IXS player lib..
*
*   Copyright (C) 2022 Juergen Wothke
*
* LICENSE
*
*   This software is licensed under a CC BY-NC-SA
*   (http://creativecommons.org/licenses/by-nc-sa/4.0/).
*/

#define _AFXDLL
#define _X86_


#include "basetypes.h"
#include "winaudio.h"
#include "File0.h"
#include "PlayerIXS.h"

//#include <string>
//#include <fstream>


using namespace IXS;

void loadIxsSong(uint *size, byte **data) {
  File0* fileMap0 = IXS__File__Z_ctor_FUN_004138e0();

#ifndef LINUX
  std::string path = "N:\\finished\\Chiptune stuff\\Music\\iXalance\\";

  std::string songs[] = {
//    "adagio for softsynths.ixs",
//    "andes.ixs",
//    "archon (vectorworld ii).ixs",
//    "caribbean.ixs",
//    "cave.ixs",
//    "christmasbells.ixs",
//    "city bells.ixs",
//    "coral reef.ixs",
//    "fly my love.. fly away!.ixs",
//    "inverse cinematics.ixs",
//    "iron dimension.ixs",
//    "ixalance theme.ixs",
//    "join the network!.ixs",
//    "movement of the gods.ixs",
//    "pacmania.it",
//    "paranoia.it",
//    "the next e.ixs",
//    "the second message.ixs",
//    "vectorworld.ixs",
//    "vixen - heineken.ixs",
    "world of noise.ixs"
  };

  FileMap* fileMap = (*fileMap0->vftable->initFileMap)(fileMap0,(char*)(path + songs[0]).c_str());
#else
  FileMap* fileMap = (*fileMap0->vftable->initFileMap)(fileMap0,(char*)"archon.ixs");
#endif

  if (fileMap == (FileMap *) nullptr) {
    fprintf(stderr, "error: cannot load song\n");
    return;
  }
  *data= fileMap->vftable->getMemBuffer(fileMap);
  *size= fileMap->vftable->getFileSize(fileMap);
}


int main()
{
  uint fileSize;
  byte *fileData;
  loadIxsSong(&fileSize, &fileData);

  uint sampleRate = 48000;  // ignored for MM audio output
  float progress = 0;
  PlayerIXS *player = IXS__PlayerIXS__createPlayer_00405d90(sampleRate);

  char r = (player->vftable->loadIxsFileData)(player, fileData, fileSize, 0, 0, &progress);
  if (r == 0) {
    char *songname= (player->vftable->getSongTitle)(player);
    fprintf(stderr, "song ready: %s\n", songname);

    (player->vftable->initAudioOut)(player);  // depends on loaded song

#if !defined(LINUX)
    // use the original VisualStudio/mmeapi/multi-thraded impl to play the
    // song on the PC's sound card

//    (player->vftable->setVolume)(0.5, 0.5);

    int i= 0;
    while (true) {

      if ((player->vftable->isSongEnd)(player)) {
        (player->vftable->stopGenAudioThread)(player);

        fprintf(stderr, "song ended\n");
        break;
      }
      if (i++ > 10) {
        i= 0;
        double p= (player->vftable->getWaveProgress)(player);
//        fprintf(stdout, "playtime: %f\n", (float)p);
      }
      Sleep(100);
    }
#else
    // single-threaded player directly outputs sequential chunks of
    // audio into the same buffer. "genAudio" gets one respective
    // chunk (this is the approach used for Web browser scenario..)

    FILE *fp= fopen("./audio.bin","wb");

    if (!fp) {
      fprintf(stderr, "error: no file\n");
      exit(1);
    }

    for (int i= 0; i<8000; i++) {
      (player->vftable->genAudio)(player);

      byte *audioBuf= (player->vftable->getAudioBuffer)(player);

      uint is16bit = (player->vftable->isAudioOutput16Bit)(player);
      uint isStereo = (player->vftable->isAudioOutputStereo)(player);

      uint len = (player->vftable->getAudioBufferLen)(player);
      len *= is16bit ? 2 : 1;
      len *= isStereo ? 2 : 1;

      fwrite(audioBuf,len,1,fp);
    }
    fclose(fp);
#endif

  }
}
