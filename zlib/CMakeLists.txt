cmake_minimum_required(VERSION 3.21)
project(zlib C)

set(CMAKE_C_STANDARD 99)

add_library(zlib
        adler32.c
        compress.c
        crc32.c
        deflate.c
        gzio.c
        infblock.c
        infcodes.c
        inffast.c
        inflate.c
        inftrees.c
        infutil.c
        trees.c
        uncompr.c
        zutil.c
        )

