# WebIXS

Copyright (C) 2022 Juergen Wothke

Copyright (C) original x86 code: Shortcut Software Development BV


WebIXS is based on "IXSPlayer Version 1.20" originally developped by Rogier, Maarten, 
Jurjen, Patrick (and others?) for "Shortcut Software Development BV" at the start of 
the millenium:

![IXSPlayer](IXSPlayer.png)

It is a "pimped up" Impulse Tracker that allows to use very small music files thanks to 
the use of compression and synthetic sample generation (in addition to its proprietary 
*.ixs format it also still is capable of playing *some* of the original 
IT format files).

Unfortunately the original source code seems to have been lost and the above shown Windows 
player executable was all that was left.


## Known limitations

I completely reverse engineered the respective code from that executable. Though originally a 
C++ program the reverse engineered code is basically C code: 
- Due to the reverse engineering from x86 ASM, large parts of the code still look quite 
obfuscated. (If anyone familiar with respective IT players wants to help "un-obfuscate" that 
code then his help is certainly most welcome.)  The memory addresses where I found respective 
code in the original x86 program are still reflected in the function/variable names (this 
was useful while still jumping back and forth between the C code and the original x86 
assembly - but now that the code seems to work fine, those names could be cleaned up). 
- I have already started to somewhat clean up the code and at least group stuff that would 
belong into separate C++ classes. There is more work to turn it into C++ code.
- Also some abstractions (and countless variables) still bear the bad names that I had 
chosen in the "discovery phase" when first trying to make sense of them. (Or that Ghidra
automatically assigned to them.)
- Certain funtions (that Ghidra had struggled with) use the "original" stack frame and the
respective "local" structs should better be extracted into individual variables.
- Though I already fixed a number of memory leaks of the original implementation I expect
there be more of those. A respective cleanup would also need to verify if destructors are
always properly called before deleting objects.
- see "todo" comments in the source code


## Usage

This project allows to build the code in three flavours:
1) The "original" multi-threaded win32 player configuration can be built using a respective "\Microsoft Visual Studio\2019\Community" environment (see CMakeLists.txt). I had used the CLion IDE and the respective project files may or may not need to be adjusted when used on a different machine (since my trial version expired I am no longer maintaining this environment).
2) The Makefile can be used on Linux to build a 32-bit command line tool that will output audio data into a file.
3) To build the JavaScript/WebAssembly Web version check out the "emscripten" folder. This is what this project is really about and for me the above variants where just means to that end.


## Background information

The original owner gave me permission to use my reverse-engineered code:

<code>Just to be clear, in case you still manage to decompile or reconstruct something, 
or get any useful details from Maarten: feel 100% free to use it however you can. Regarding 
the intellectual property rights that are officially ours, I revoke any claims beforehand.</code>

In the spirit of a non-commercial hobby project I am therefore releasing my project
using the below license so that other hobbyists can similarly benefit from the code.
 
## License
This software is licensed under a CC BY-NC-SA
(http://creativecommons.org/licenses/by-nc-sa/4.0/).
