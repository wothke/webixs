# WebIXS

Copyright (C) 2022 Juergen Wothke

This plugin is designed to work with my generic WebAudio ScriptProcessor music player (see separate project)
but the API exposed by the lib can be used in any JavaScipt program (it should look familiar to anyone 
that has ever done some sort of music player plugin). 
A working demo can be found here: <a href="https://www.wothke.ch/webixs" target="blank">webIxalance</a>

PS: I noticed that the "newer" JavaScript "class" syntax which I am using in parts of
my online demo page does not seem to be handled by my older Firefox browser, i.e.
you may need a recent Web browser version to run the page.


![IXSPlayer](screenshot.gif)



## Howto build

You'll need Emscripten (http://kripken.github.io/emscripten-site/docs/getting_started/downloads.html). 
The make scripts are designed for use of emscripten version 1.37.29 (unless you want to 
create WebAssembly output, older versions might also still work).

The below instructions assume that you have opened a Windows "cmd" window and that the Emscripten 
environment vars have already been set (e.g. run emsdk_env.bat from within your Emscripten 
installation folder).

Now change the current directory (i.e. "cd") to the project's "webixs/emscripten" 
folder. The Web version is built using the make.bat and makeWorker.bat scripts that can 
be found in this folder. The scripts will compile directly into the "emscripten/htdocs" 
example web sub-folder, were it will create the backend_ixs.js, ixs.wasm & wavegen_ixs.js libraries. 
The content of the "htdocs" can be tested by copying it into some document folder on 
a web server (you'll still need song files).


## Dependencies

The current version requires version 1.1.4 of my https://github.com/wothke/webaudio-player.


## Background information

To improve UI responsiveness of the Web version, the "expensive" (slow) audio pre-
rendering logic has been decoupled from the main player program and put into an 
asynchronous Worker thread. Also the Web browser's "DB feature" is used to cache 
pre-rendered files such that successive starts of the same song should be much faster.

Unfortunately it seems that the Google clowns once again fucked up and the respective
infrastructure actually risks to make the user experience worse: While it works, 
the page's behavior is actually nicer - however I have observed instances where 
the whole page becomes completely unresponsive right after page-load. I did not 
investigate but I suspect that it might be the "browser's DB" that sporadically 
just blocks the whole browser tab for several minutes.


 
## License

 This software is licensed under a CC BY-NC-SA
 (http://creativecommons.org/licenses/by-nc-sa/4.0/).
