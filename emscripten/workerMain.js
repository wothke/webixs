/* todo: this might be folded into workerWrapEnd.js */

// worker is started here and it responds via code in callbackWaveGen.js
onmessage = function(oEvent) {
    if (oEvent.data instanceof Object && oEvent.data.hasOwnProperty('generate')) {
        var filename = oEvent.data.generate[0];
console.log("Worker - start generation: "+filename);
        var buffer1 = oEvent.data.generate[1];
        var buffer2 = oEvent.data.generate[2];

        if(!waveGen_IXS.Module.notReady) {

            var buf1 = waveGen_IXS.Module._malloc(buffer1.length);
            waveGen_IXS.Module.HEAPU8.set(buffer1, buf1);
            var buf2 = waveGen_IXS.Module._malloc(buffer2.length);
            waveGen_IXS.Module.HEAPU8.set(buffer2, buf2);

            // note: the "file save" is triggered by the called C++ code
            // via "JS_saveCacheFile" (see callbackWaveGen.js)
            var ret = waveGen_IXS.Module.ccall('emu_create_sample_cache_file', 'number',
                                                    ['string', 'number', 'number'], [filename, buf1, buf2]);

            waveGen_IXS.Module._free(buf2);
            waveGen_IXS.Module._free(buf1);

        } else {
            postMessage({"error": "Worker - not ready"});
        }
    } else {
        postMessage({"error": "Worker -  unknown message"});
    }
};
