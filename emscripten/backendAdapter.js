/*
 ixs_adapter.js: Adapts Ixalance (IXS) backend to generic WebAudio/ScriptProcessor player.

	Known limitation: The used ScriptNodePlayer was not designed for Workers but expects
	that all file loading occurs via XHR. Consequently integrating the Worker used here
	is a hack (and the ScriptNodePlayer also had to be patched up).

	IXS does not need to load separate files but instead generates a "cached sample file"
	during load the respective "Worker" based laod is somewhat hacked into this impl..

 	Copyright (C) 2022-2023 Juergen Wothke

 LICENSE

	 This software is licensed under a CC BY-NC-SA
	 (http://creativecommons.org/licenses/by-nc-sa/4.0/).
*/

class IXSBackendAdapter extends EmsHEAP16BackendAdapter {
	constructor(logCallback)
	{
		super(backend_IXS.Module, 2, new SimpleFileMapper(backend_IXS.Module),
					new HEAP16ScopeProvider(backend_IXS.Module, 0x8000));

		this._log = typeof logCallback == 'function' ? logCallback : function(ignoreMsg) {};
		this._songSize = 0;

		this.setProcessorBufSize(16384/4);	// "the x.whales" does not like smaller buffer

		let path = 'wavegen_ixs.js';
		path =  (typeof window.WASM_SEARCH_PATH == 'undefined') ? path : window.WASM_SEARCH_PATH + path;
		
		this.wavegenWorker = new Worker(path);
		this.wavegenWorker.onmessage = function(oEvent) {

			if (oEvent.data instanceof Object)
			{
				if (oEvent.data.hasOwnProperty('logMsg'))
				{
					this._log(oEvent.data.logMsg);

				}
				else if (oEvent.data.hasOwnProperty('cacheReady'))
				{
					// like the async notification of a XHR response.. now the
					// player can be triggered to "retry"
					let filename = oEvent.data.cacheReady;
					this._loadFileDataFromDB(filename);
				}
				else
				{
					// above should be the only messages
				}
			}

		}.bind(this);
		// must unfortunately be registered as a global var so that the
		// code in callback.js can access it..
		window.wavegenWorker = this.wavegenWorker;

		this.ensureReadyNotification();
	}

	// using the browser's DB feature to store the generated "temp cache files"
	// so that all future page loads are much faster
	_loadFileDataFromDB(filename)
	{
		window.request = indexedDB.open('ixs_cache_db', 3);
		window.request.onupgradeneeded = function(e) {
			let db = e.target.result;

			if (e.oldVersion > 0)
			{
				db.deleteObjectStore('ixs_cache_os');
			}

			let objectStore = db.createObjectStore('ixs_cache_os', { keyPath: 'id', autoIncrement:true });

			objectStore.createIndex('filename', 'filename', { unique: true });
			objectStore.createIndex('data', 'data', { unique: false });
		};
		window.request.onsuccess = function() {
			let db = window.request.result;

			let store = db.transaction('ixs_cache_os').objectStore('ixs_cache_os');
			let index = store.index("filename");

			let r = index.get(filename);
			r.onsuccess = function() {
				let matching = r.result;
				if (matching !== undefined)
				{
					ScriptNodePlayer.getInstance().asyncSetFileData(filename, matching.data);
				}
				else
				{
					console.log("ERROR: advertised DB entry not found: "+filename);
				}
			};
		};
		return 0;	// data isn't available yet and a "retry" is needed later
	}

	loadMusicData(sampleRate, path, filename, data, options)
	{
		let p = ScriptNodePlayer.getInstance();
		// what ugly garbage! but it is needed for access from callback.js :-(
		window["ixs_getCached"] = p.getCached.bind(p);
		window["ixs_asyncSetFileData"] = p.asyncSetFileData.bind(p);
		window["ixs_HEAP8"] = this.Module.HEAP8;

		this._songSize = data.length;


		filename = this._getFilename(path, filename);

		let ret = this._loadMusicDataBuffer(filename, data, ScriptNodePlayer.getWebAudioSampleRate(), -9999, true);

		if (ret == 0)
		{
			this._setupOutputResampling(sampleRate);
		}
		return ret;
	}

	updateSongInfo(filename)
	{
		this._songInfo = new Object();
		this._songInfo.title = this.Module.ccall('emu_get_song_title', 'string');;
		this._songInfo.songSize = this._songSize;
	}
/*
	getSongInfo()
	{
		return this._songInfo;
	}
*/
	getSongInfoMeta()
	{
		return {
			title: String,
			songSize: Number
		};
	}

	// in IXS context this is just used to propagate "log" messages to the UI.
	// triggered via window['songUpdateCallback'] call in callback.js
	handleBackendSongAttributes(msg)
	{
		if (typeof this._log != 'undefined')
		{
			this._log(msg);
		}
	}

	// selects the used output generation impl (high means stereo+)
	setQuality(highEnabled)
	{
		this.Module.ccall('emu_set_quality', 'number', ['number'], [highEnabled]);
	}

	// disable unsupported default impls

	getMaxPlaybackPosition()
	{
		return -1;
	}
	getPlaybackPosition()
	{
		return -1;
	}
	seekPlaybackPosition(ms)
	{
	}
}

