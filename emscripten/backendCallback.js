// additional emscripten *.js library must be in this dumbshit format..
// these are addressed as regular extern "C" functions..

mergeInto(LibraryManager.library, {
	JS_printStatus: function(msgPtr) {
		var msg= Pointer_stringify(msgPtr);
		var p = window["ScriptNodePlayer"]["getInstance"]();
		if (!p["isReady"]()) {
			window["console"]["log"]("error: JS_printStatus not ready");
		}
		else {
			return p["_songUpdateCallback"](msg);
		}
	},

	JS_getCacheFileData: function(filenamePtr, dataBuf1, lenBuf1, dataBuf2, lenBuf2, destination) {
		var filename= Pointer_stringify(filenamePtr);

		var data = window["ixs_getCached"](filename);	// hack: use player's internal cache
		if (data != null) {
			// data has been directly uploaded here from ixs_adapter.js
			var heap8= window["ixs_HEAP8"];
			for(var i= 0; i<data.length; i++) {
				heap8[destination+i]= data[i];
			}

			return data.length;
		}

		// not yet in synchronously usable cache yet (but it might be in DB)
		// todo: damn code duplication..see ixs_adapter.js

		window.request = indexedDB.open('ixs_cache_db', 3);
		window.request.onupgradeneeded = function(e) {
			var db = e.target.result;

			if (e.oldVersion > 0) {
				db.deleteObjectStore('ixs_cache_os');
			}

			var objectStore = db.createObjectStore('ixs_cache_os', { keyPath: 'id', autoIncrement:true });
			objectStore.createIndex('filename', 'filename', { unique: true });
			objectStore.createIndex('data', 'data', { unique: false });
		};
		window.request.onsuccess = function() {
			var db = window.request.result;

			var store = db.transaction('ixs_cache_os').objectStore('ixs_cache_os');
			var index = store.index("filename");

			var r = index.get(filename);
			r.onsuccess = function() {

				var matching = r.result;
				if (matching !== undefined) {
					window["ixs_asyncSetFileData"](filename, matching.data);

				} else {
					// not yet in DB
					var buffer1 = window["rawBytes"](Module, dataBuf1, lenBuf1);
					var buffer2 = window["rawBytes"](Module, dataBuf2, lenBuf2);

					window['wavegenWorker'].postMessage({"generate": [filename, buffer1, buffer2]});
				}
			};
		}

		return 0;	// data isn't available yet and a "retry" is needed later
	},
});