// create separate namespace for all the Emscripten stuff.. otherwise naming clashes may occur especially when 
// optimizing using closure compiler..
window.spp_backend_state_IXS= {
	locateFile: function(path, scriptDirectory) { return (typeof window.WASM_SEARCH_PATH == 'undefined') ? path : window.WASM_SEARCH_PATH + path; },
	notReady: true,
	adapterCallback: function(){}	// overwritten later	
};
window.spp_backend_state_IXS["onRuntimeInitialized"] = function() {	// emscripten callback needed in case async init is used (e.g. for WASM)
	this.notReady= false;
	this.adapterCallback();
}.bind(window.spp_backend_state_IXS);


// needed to pass unchanged binary data from C to JS
window.rawBytes= function(module, ptr, len) {
	var ret= [];	
	for (i = 0; i<len; i++) {
		var ch = module.getValue(ptr++, 'i8', true);
		ret.push(ch & 0xff);
	}
	return new Uint8Array(ret);
}

var backend_IXS = (function(Module) {