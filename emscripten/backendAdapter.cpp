/*
* This file adapts IXS to the interface expected by my generic JavaScript ScriptNodePlayer.
*
*   Copyright (C) 2022-2023 Juergen Wothke
*
* LICENSE
*
*   This software is licensed under a CC BY-NC-SA
*   (http://creativecommons.org/licenses/by-nc-sa/4.0/).
*/

#include "File0.h"
#include "PlayerIXS.h"

#include <emscripten.h>


using namespace IXS;


#define NUM_SCOPES 2
#define DEFAULT_SCOPE_SIZE 5000

namespace IXS {
	class Adapter {
	public:
		Adapter() : _player(nullptr), _progress(0), _is_ready(false), _scope_buffer_len(0), _is_initialized(false)
		{
		}

		int32_t loadFile(char *filename, void* in_buffer, uint32_t in_buf_size, uint32_t sample_rate, uint32_t audioBufSize, uint32_t scopesEnabled)
		{
			// fixme: audioBufSize support not implemented

			_is_ready = false;

			init(sample_rate);

			int r = (_player->vftable->loadIxsFileData)(_player, (byte*)in_buffer, in_buf_size, 0, 0, &_progress);

			if (r == 0)
			{
				(_player->vftable->initAudioOut)(_player);

				_is_ready = true;
			}

			return r;
		}

		uint32_t getSampleRate()
		{
		  return  (_player->vftable->getSampleRate)();
		}

		int genSamples() {
			if (!_is_ready) return 0;  // just ignore

			if (!(_player->vftable->isSongEnd)(_player))
			{
				(_player->vftable->genAudio)(_player);
				return 0;
			}
			return 1; // song is done
		}

		char* getSampleBuffer()
		{
		  return (char*)(_player->vftable->getAudioBuffer)(_player);
		}

		long getSampleBufferLength()
		{
		  uint is16bit = (_player->vftable->isAudioOutput16Bit)(_player);
		  uint isStereo = (_player->vftable->isAudioOutputStereo)(_player);

		  uint len = (_player->vftable->getAudioBufferLen)(_player);
		  len *= is16bit ? 2 : 1;
		  len *= isStereo ? 2 : 1;

		  return len >> 2;
		}

		char* getSongTitle()
		{
		  return (char*)(_player->vftable->getSongTitle)(_player);
		}

		void setQuality(uint highEnabled)
		{
			(_player->vftable->setMixer)(_player, highEnabled);
		}

		int getNumberTraceStreams()
		{
			return NUM_SCOPES;
		}

		const char** getTraceStreams()
		{
			uint is16bit = (_player->vftable->isAudioOutput16Bit)(_player);
			uint isStereo = (_player->vftable->isAudioOutputStereo)(_player);
			if (!is16bit || !isStereo)
			{
				fprintf(stderr, "error: unexpected audio format %d %d\n", is16bit, isStereo);
				return 0;
			}

			uint len = (_player->vftable->getAudioBufferLen)(_player);
			if (len && (_scope_buffer_len < len))
			{
				for(int i= 0; i < NUM_SCOPES; i++) {
					if (_scope_buffers[i])
					{
						_scope_buffers[i] = (short*)realloc((void*)_scope_buffers[i], len * sizeof(short));
					}
				}
				_scope_buffer_len = len;
			}

			short *interleaved = (short*)getSampleBuffer();
			short *left = _scope_buffers[0];
			short *right = _scope_buffers[1];

			for (int i= 0; i<len; i++) {
				*left++= *interleaved++;
				*right++= *interleaved++;
			}

			return (const char**)_scope_buffers;	// ugly cast to make emscripten happy
		}

	private:
		int init(uint sample_rate)
		{
		  if (!_is_initialized)
		  {
			_player = IXS__PlayerIXS__createPlayer_00405d90(sample_rate);

			for(int i= 0; i<NUM_SCOPES; i++) {
				_scope_buffers[i] = (short*)malloc(DEFAULT_SCOPE_SIZE*sizeof(short));
			}
			_scope_buffer_len = DEFAULT_SCOPE_SIZE;

			_is_initialized = true;
		  }
		  return 0;
		}

	private:
		PlayerIXS *_player;
		float _progress;
		bool _is_ready;

		int16_t* 	_scope_buffers[NUM_SCOPES];
		uint     	_scope_buffer_len;

		bool _is_initialized;
	};
};

IXS::Adapter _adapter;



// old style EMSCRIPTEN C function export to JavaScript.
// todo: code might be cleaned up using EMSCRIPTEN's "new" Embind feature:
// https://emscripten.org/docs/porting/connecting_cpp_and_javascript/embind.html
#define EMBIND(retType, func)  \
	extern "C" retType func __attribute__((noinline)); \
	extern "C" retType EMSCRIPTEN_KEEPALIVE func

// --- standard functions
EMBIND(int, emu_load_file(char *filename, void *inBuffer, uint32_t inBufSize, uint32_t sampleRate, uint32_t audioBufSize, uint32_t scopesEnabled)) {
	return _adapter.loadFile(filename, inBuffer, inBufSize, sampleRate, audioBufSize, scopesEnabled); }
EMBIND(void, emu_teardown())						{ /* add cleanup code here? */}
EMBIND(int, emu_get_sample_rate())					{ return _adapter.getSampleRate(); }
EMBIND(int, emu_compute_audio_samples())			{ return _adapter.genSamples(); }
EMBIND(char*, emu_get_audio_buffer())				{ return _adapter.getSampleBuffer(); }
EMBIND(int, emu_get_audio_buffer_length())			{ return _adapter.getSampleBufferLength(); }
EMBIND(int, emu_number_trace_streams())				{ return _adapter.getNumberTraceStreams(); }
EMBIND(const char**, emu_get_trace_streams())		{ return _adapter.getTraceStreams(); }

// --- add-on
EMBIND(char*, emu_get_song_title())					{ return _adapter.getSongTitle(); }
EMBIND(void, emu_set_quality(uint highEnabled))		{ _adapter.setQuality(highEnabled); }
