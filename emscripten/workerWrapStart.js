// create separate namespace for all the Emscripten stuff.. otherwise naming clashes may occur especially when 
// optimizing using closure compiler..
self.spp_ixs_worker = {
	locateFile: function(path, scriptDirectory) { return (typeof window.WASM_SEARCH_PATH == 'undefined') ? path : window.WASM_SEARCH_PATH + path; },
	notReady: true,
	adapterCallback: function(){}	// overwritten later	
};
self.spp_ixs_worker["onRuntimeInitialized"] = function() {	// emscripten callback needed in case async init is used (e.g. for WASM)
	this.notReady= false;
	this.adapterCallback();
}.bind(self.spp_ixs_worker);

// needed to pass unchanged binary data from C to JS
self.rawBytes= function(module, ptr, len) {
	var ret= [];
	for (i = 0; i<len; i++) {
		var ch = module.getValue(ptr++, 'i8', true);
		ret.push(ch & 0xff);
	}
	return new Uint8Array(ret);
}

var waveGen_IXS = (function(Module) {