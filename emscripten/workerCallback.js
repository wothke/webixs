// additional emscripten *.js library must be in this dumbshit format..
// these are functions that are directly called from the C++/C code (via extern "C")

mergeInto(LibraryManager.library, {
	JS_printStatus: function(msg) {
		var msgText= Pointer_stringify(msg);
		postMessage({"logMsg": msgText})	// async propagation
	},


	// save the file to the browser's DB
	JS_saveCacheFile: function(filename0, dataBuf, lenBuf) {
		var filename= Pointer_stringify(filename0);
		var buffer = self["rawBytes"](Module, dataBuf, lenBuf);

		self.request = indexedDB.open('ixs_cache_db', 3);
		self.request.onupgradeneeded = function(e) {
			var db = e.target.result;

			if (e.oldVersion > 0) {
				db.deleteObjectStore('ixs_cache_os');
			}

			var objectStore = db.createObjectStore('ixs_cache_os', { keyPath: 'id', autoIncrement:true });

			objectStore.createIndex('filename', 'filename', { unique: true });
			objectStore.createIndex('data', 'data', { unique: false });
		};
		self.request.onsuccess = function() {
			var db = self.request.result;

			var newItem = { filename: filename, data: buffer };
			var transaction = db.transaction(['ixs_cache_os'], 'readwrite');
			var objectStore = transaction.objectStore('ixs_cache_os');
			var r = objectStore.add(newItem);
			r.onsuccess = function() {
			};
			r.onerror = function(a, e) {
			}
			transaction.oncomplete = function() {
				postMessage({"cacheReady": filename});
			};
		};
	},

});