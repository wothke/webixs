:: build Worker for WaveGen functionality

:: NOTE: this could probably also be built as WASM but I don't want to take the risk of having any
:: async "is ready" issues in the Worker context.. (since this only runs "once" initially it just
:: isn't worth it..)

call emcc.bat -s ASSERTIONS=0 -s SAFE_HEAP=0 -s VERBOSE=0 -s FORCE_FILESYSTEM=0 -Wno-pointer-sign ../src/WaveLibrary.c ../src/FileMap.cpp ../src/FileMapSFXI.cpp ../src/FileSFXI.cpp ../src/WaveGen.cpp ../src/asmEmu.cpp -o built/ixs_wavegen.bc
emcc.bat  -s WASM=0 -s ASSERTIONS=0 -s SAFE_HEAP=0 -s TOTAL_MEMORY=67108864 --js-library workerCallback.js -s VERBOSE=0 -s FORCE_FILESYSTEM=0 -Wno-pointer-sign -I../src/ -Os -O3 --memory-init-file 0 --closure 1 --llvm-lto 1   built/ixs_wavegen.bc workerAdapter.cpp -s EXPORTED_FUNCTIONS="['_emu_create_sample_cache_file', '_malloc', '_free', '_ldexp']"  -o htdocs/ixsWaveGen.js -s SINGLE_FILE=0 -s EXTRA_EXPORTED_RUNTIME_METHODS="['ccall', 'getValue', 'Pointer_stringify']"  -s BINARYEN_ASYNC_COMPILATION=1 -s BINARYEN_TRAP_MODE='clamp' && copy /b workerWrapStart.js + htdocs\ixsWaveGen.js + workerWrapEnd.js htdocs\web_ixsWaveGen.js && del htdocs\ixsWaveGen.js && copy /b htdocs\web_ixsWaveGen.js + workerMain.js htdocs\wavegen_ixs.js && del htdocs\web_ixsWaveGen.js

:END

