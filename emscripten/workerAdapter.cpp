/*
* This file provides the entry point to use WaveGen as a Web Worker.
*
*   Copyright (C) 2022 Juergen Wothke
*
* LICENSE
*
*   This software is licensed under a CC BY-NC-SA
*   (http://creativecommons.org/licenses/by-nc-sa/4.0/).
*/

#include "File0.h"
#include "WaveGen.h"

#include <emscripten.h>


using namespace IXS;


extern "C" int emu_create_sample_cache_file(const char *filename, byte *dataBuf1, byte *dataBuf2) __attribute__((noinline));
extern "C" int EMSCRIPTEN_KEEPALIVE emu_create_sample_cache_file(const char *filename, byte *dataBuf1, byte *dataBuf2)
{
  return IXS__WAVEGEN__createSampleCacheFile((char*)filename, dataBuf1, dataBuf2, nullptr);
}

