:: build the main "backend" library (the Worker must be build separately via makeWorker.bat)

set "OPT= -s WASM=1 -s ASSERTIONS=0 -s SAFE_HEAP=0 -s VERBOSE=0 -s FORCE_FILESYSTEM=0 -Wno-pointer-sign -I../src/ -I../zlib -Os  -O3  "

::if not exist "built/ixs.bc" (
	call emcc.bat %OPT% ../src/WaveLibrary.c ../src/winaudio.cpp ../src/Packer.cpp ../src/FileMap.cpp ../src/File0.cpp ../src/FileMapSFXI.cpp ../src/FileSFXI.cpp ../src/Module.cpp ../src/PlayerIXS.cpp ../src/PlayerCore.cpp ../src/MixerBase.cpp ../src/Mixer1.cpp ../src/Mixer2.cpp ../src/Mixer3.cpp ../src/Mixer4.cpp ../src/asmEmu.cpp -o built/ixs.bc
::	IF !ERRORLEVEL! NEQ 0 goto :END
::)

::if not exist "built/zlib.bc" (
	call emcc.bat %OPT%  ../zlib/adler32.c ../zlib/compress.c ../zlib/crc32.c ../zlib/deflate.c ../zlib/gzio.c ../zlib/infblock.c ../zlib/infcodes.c ../zlib/inffast.c ../zlib/inflate.c ../zlib/inftrees.c ../zlib/infutil.c ../zlib/trees.c ../zlib/uncompr.c ../zlib/zutil.c  -o built/zlib.bc
::	IF !ERRORLEVEL! NEQ 0 goto :END
::)

emcc.bat  -s WASM=1 -s ASSERTIONS=0 -s SAFE_HEAP=0 -s TOTAL_MEMORY=335544320 --js-library backendCallback.js -s VERBOSE=0 -s FORCE_FILESYSTEM=0 -Wno-pointer-sign -I../src/ -I../zlib/  -Os -O3 --memory-init-file 0 --closure 1 --llvm-lto 1    built/zlib.bc built/ixs.bc backendAdapter.cpp -s EXPORTED_FUNCTIONS="['_emu_get_sample_rate','_emu_load_file','_emu_compute_audio_samples','_emu_teardown','_emu_get_audio_buffer','_emu_get_audio_buffer_length','_emu_get_song_title','_emu_number_trace_streams','_emu_get_trace_streams','_emu_set_quality', '_malloc', '_free', '_ldexp']"  -o htdocs/ixs.js -s SINGLE_FILE=0 -s EXTRA_EXPORTED_RUNTIME_METHODS="['ccall', 'getValue', 'Pointer_stringify']"  -s BINARYEN_ASYNC_COMPILATION=1 -s BINARYEN_TRAP_MODE='clamp' && copy /b backendWrapStart.js + htdocs\ixs.js + backendWrapEnd.js htdocs\web_ixs.js && del htdocs\ixs.js && copy /b htdocs\web_ixs.js + backendAdapter.js htdocs\backend_ixs.js && del htdocs\web_ixs.js

:END

