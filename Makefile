# quick hack for testing on linux..

CC = gcc  -m32 -DLINUX=1
CXX = g++  -m32 -DLINUX=1 -Dnullptr=0 -fpermissive -std=c++11  -fno-exceptions
CFLAGS = -g -MMD

LDFLAGS += -lm -lstdc++

all: ixs


zlibobjs = ./zlib/adler32.o ./zlib/compress.o ./zlib/crc32.o ./zlib/deflate.o ./zlib/gzio.o ./zlib/infblock.o ./zlib/infcodes.o ./zlib/inffast.o ./zlib/inflate.o ./zlib/inftrees.o ./zlib/infutil.o ./zlib/trees.o ./zlib/uncompr.o ./zlib/zutil.o

zlib: $(zlibobjs)
	ar rcs libzlib.a  $(zlibobjs)

$(zlibobjs): %.o: %.c


ixsobjs1 = ./src/WaveLibrary.o
ixsobjs2 = ./src/winaudio.o ./src/Packer.o ./src/FileMap.o ./src/File0.o ./src/FileMapSFXI.o ./src/FileSFXI.o ./src/Module.o ./src/WaveGen.o ./src/PlayerIXS.o ./src/PlayerCore.o ./src/MixerBase.o ./src/Mixer1.o ./src/Mixer2.o ./src/Mixer3.o ./src/Mixer4.o ./src/asmEmu.o

$(ixsobjs1): %.o: %.c
$(ixsobjs2): %.o: %.cpp

ixslib: $(ixsobjs1) $(ixsobjs2)
	ar rcs ixslib.a  $(ixsobjs1) $(ixsobjs2)


mainobj = ./src/main.o
$(mainobj): %.o: %.cpp

		
ixs: ./src/main.cpp $(ixsobjs1) $(ixsobjs2) $(zlibobjs)
	$(CXX) ./src/main.cpp $(ixsobjs1) $(ixsobjs2) $(zlibobjs) -o $@ $(LDFLAGS)
		
		
clean: 
	rm ./src/*.o ./zlib/*.o ./*.a ixs